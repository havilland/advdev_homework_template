#!/bin/bash
# Setup Production Project (initial active services: Green)
if [ "$#" -ne 1 ]; then
    echo "Usage:"
    echo "  $0 GUID"
    exit 1
fi

GUID=$1
echo "Setting up Parks Production Environment in project ${GUID}-parks-prod"

# Code to set up the parks production project. It will need a StatefulSet MongoDB, and two applications each (Blue/Green) for NationalParks, MLBParks and Parksmap.
# The Green services/routes need to be active initially to guarantee a successful grading pipeline run.

# To be Implemented by Student

# Grant jenkins access to project
oc policy add-role-to-user edit system:serviceaccount:${GUID}-jenkins:jenkins -n ${GUID}-parks-prod

# Create mongodb services
oc create -f ../templates/mongo-headless-service.yml -n ${GUID}-parks-prod
oc create -f ../templates/mongodb-service.yml -n ${GUID}-parks-prod

# Create mongodb statefulset
oc create -f ../templates/mongodb-template.yml -n ${GUID}-parks-prod

# Grant access in the dev project
oc policy add-role-to-group system:image-puller system:serviceaccounts:${GUID}-parks-prod -n ${GUID}-parks-dev

# Create config for mongodb access
oc create configmap mongodb-settings --from-literal=DB_HOST=mongodb --from-literal=DB_PORT=27017 --from-literal=DB_USERNAME=mongodb --from-literal=DB_PASSWORD=mongodb --from-literal=DB_NAME=parks --from-literal=DB_REPLICASET=rs0 -n ${GUID}-parks-prod
oc set env --from=configmap/mongodb-settings dc/mlbparks-green -n ${GUID}-parks-prod
oc set env --from=configmap/mongodb-settings dc/mlbparks-blue -n ${GUID}-parks-prod

oc set probe dc/mlbparks-blue --readiness --get-url=http://:8080/ws/healthz/ --initial-delay-seconds=15 -n ${GUID}-parks-prod
oc set deployment-hook dc/mlbparks-blue -n ${GUID}-parks-prod --post -- curl http://nationalparks.${GUID}-parks-prod.svc:8080/ws/data/load/