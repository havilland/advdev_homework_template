#!/bin/bash
# Setup Development Project
if [ "$#" -ne 1 ]; then
    echo "Usage:"
    echo "  $0 GUID"
    exit 1
fi

GUID=$1
echo "Setting up Parks Development Environment in project ${GUID}-parks-dev"

# Code to set up the parks development project.

# To be Implemented by Student

# Grant jenkins access to the Project
oc policy add-role-to-user edit system:serviceaccount:${GUID}-jenkins:jenkins -n ${GUID}-parks-dev
oc policy add-role-to-user view --serviceaccount=default -n ${GUID}-parks-dev

# Create mongodb from template
oc new-app --template=mongodb-persistent-template -p MONGODB_USER=mongodb -p MONGODB_PASSWORD=mongodb -p MONGODB_DATABASE=parks -p MONGODB_ADMIN_PASSWORD=mongodbadmin -n ${GUID}-parks-dev

# Create build-configs for all 3 parts of the application
oc new-build --binary=true --name="mlbparks" jboss-eap70-openshift:1.7 -n ${GUID}-parks-dev

# Create deploymentconfigs for all parts of the application
oc new-app ${GUID}-parks-dev/mlbparks:0.0-0 --name=mlbparks --allow-missing-imagestream-tags=true -n ${GUID}-parks-dev

# Create Configmap for mlbparks and use it as env
oc create configmap mlbparks --from-literal=APPNAME="MLB Parks (Dev)" -n ${GUID}-parks-dev
oc set env --from=configmap/mlbparks dc/mlbparks -n ${GUID}-parks-dev

# Create config for mongodb access
oc create configmap mongodb-settings --from-literal=DB_HOST=mongodb --from-literal=DB_PORT=27017 --from-literal=DB_USERNAME=mongodb --from-literal=DB_PASSWORD=mongodb --from-literal=DB_NAME=parks -n ${GUID}-parks-dev
oc set env --from=configmap/mongodb-settings dc/mlbparks -n ${GUID}-parks-dev

# Expose mlbparks service with label
oc expose svc/mlbparks --labels='type=parksmap-backend' -n ${GUID}-parks-dev

oc set probe dc/mlbparks --readiness --get-url=http://:8080/ws/healthz/ --initial-delay-seconds=15 -n ${GUID}-parks-dev
oc set deployment-hook dc/mlbparks -n ${GUID}-parks-dev --post -- curl http://mlbparks.${GUID}-parks-dev.svc:8080/ws/data/load/

# Set up National Parks Backend
oc new-build --binary=true --name="nationalparks" redhat-openjdk18-openshift:1.2 -n ${GUID}-parks-dev
oc new-app ${GUID}-parks-dev/nationalparks:0.0-0 --name=nationalparks --allow-missing-imagestream-tags=true -n ${GUID}-parks-dev
# Create Configmap for nationalparks and use it as env
oc create configmap nationalparks --from-literal=APPNAME="National Parks (Dev)" -n ${GUID}-parks-dev
oc set env --from=configmap/nationalparks dc/nationalparks -n ${GUID}-parks-dev

# Create config for mongodb access
oc set env --from=configmap/mongodb-settings dc/nationalparks -n ${GUID}-parks-dev

# Expose nationalparks service with label
oc expose svc/nationalparks --labels='type=parksmap-backend' -n ${GUID}-parks-dev

oc set probe dc/nationalparks --readiness --get-url=http://:8080/ws/healthz/ --initial-delay-seconds=15 -n ${GUID}-parks-dev
oc set deployment-hook dc/nationalparks -n ${GUID}-parks-dev --post -- curl http://nationalparks.${GUID}-parks-dev.svc:8080/ws/data/load/

# Setup Frontend Service
oc new-build --binary=true --name="parksmap" redhat-openjdk18-openshift:1.2 -n ${GUID}-parks-dev
oc new-app ${GUID}-parks-dev/parksmap:0.0-0 --name=parksmap --allow-missing-imagestream-tags=true -n ${GUID}-parks-dev