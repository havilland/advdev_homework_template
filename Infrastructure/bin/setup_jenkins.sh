#!/bin/bash
# Setup Jenkins Project
if [ "$#" -ne 3 ]; then
    echo "Usage:"
    echo "  $0 GUID REPO CLUSTER"
    echo "  Example: $0 wkha https://github.com/wkulhanek/ParksMap na39.openshift.opentlc.com"
    exit 1
fi

GUID=$1
REPO=$2
CLUSTER=$3
echo "Setting up Jenkins in project ${GUID}-jenkins from Git Repo ${REPO} for Cluster ${CLUSTER}"

# Code to set up the Jenkins project to execute the
# three pipelines.
# This will need to also build the custom Maven Slave Pod
# Image to be used in the pipelines.
# Finally the script needs to create three OpenShift Build
# Configurations in the Jenkins Project to build the
# three micro services. Expected name of the build configs:
# * mlbparks-pipeline
# * nationalparks-pipeline
# * parksmap-pipeline
# The build configurations need to have two environment variables to be passed to the Pipeline:
# * GUID: the GUID used in all the projects
# * CLUSTER: the base url of the cluster used (e.g. na39.openshift.opentlc.com)

#oc new-app jenkins-persistent --param ENABLE_OAUTH=true --param MEMORY_LIMIT=2Gi --param VOLUME_CAPACITY=4Gi -n $GUID-jenkins

oc project ${GUID}-jenkins

echo "FROM docker.io/openshift/jenkins-slave-maven-centos7:v3.9
USER root
RUN yum -y install skopeo apb && \
    yum clean all
USER 1001" | oc new-build --dockerfile -

oc process -f ../templates/jenkins-maven-slave.yml --param GUID=${GUID} --param JENKINS_APPLICATION=jenkins-persistent | oc create -n ${GUID}-jenkins -f -

oc new-app -f ../templates/jenkins-template.json  --param ENABLE_OAUTH=true --param MEMORY_LIMIT=2Gi --param VOLUME_CAPACITY=4Gi -n ${GUID}-jenkins

echo 'kind: "BuildConfig"
apiVersion: "v1"
metadata:
  name: "mlbparks-pipeline"
spec:
  source:
    git:
      uri: '${REPO}'
  strategy:
    jenkinsPipelineStrategy:
      jenkinsfilePath: MLBParks/Jenkinsfile' | oc create -n ${GUID}-jenkins -f - 

oc env bc/mlbparks-pipeline GUID=${GUID} CLUSTER=${CLUSTER} -n ${GUID}-jenkins

echo 'kind: "BuildConfig"
apiVersion: "v1"
metadata:
  name: "nationalparks-pipeline"
spec:
  source:
    git:
      uri: '${REPO}'
  strategy:
    jenkinsPipelineStrategy:
      jenkinsfilePath: Nationalparks/Jenkinsfile' | oc create -n ${GUID}-jenkins -f - 

oc env bc/nationalparks-pipeline GUID=${GUID} CLUSTER=${CLUSTER} -n ${GUID}-jenkins

echo 'kind: "BuildConfig"
apiVersion: "v1"
metadata:
  name: "parksmap-pipeline"
spec:
  source:
    git:
      uri: '${REPO}'
  strategy:
    jenkinsPipelineStrategy:
      jenkinsfilePath: ParksMap/Jenkinsfile' | oc create -n ${GUID}-jenkins -f - 

oc env bc/parksmap-pipeline= GUID=${GUID} CLUSTER=${CLUSTER} -n ${GUID}-jenkins